(ns server.core
  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.pprint :refer [pprint]]
            [ring.adapter.jetty :as jetty]
            [ring.middleware.reload :refer [wrap-reload]]
            [ring.middleware.resource :refer [wrap-resource]]
            [ring.util.response :refer [response 
                                        response?
                                        content-type 
                                        redirect 
                                        file-response 
                                        resource-response]]
            [reitit.ring :as reitit-ring]
            [server.routes :refer [router]]))

(defn prettify
  "Outputs a prettified string of the request"
  [request]
  (with-out-str (pprint request)))

;;;; Handlers

(defn reflect-handler
  [request]
  {:status 200
   :headers {"Content-Type" "text/plain"}
   :body (prettify request)})

(defn resource-handler
  [request]
  (as-> request r
    (:path-params r)
    (:filename r)
    {:status 200
     :body (io/input-stream (str "public/index.html" r))}))

(defn index-handler
  [request]
  (file-response "index.html" {:root "public"}))
  ;(resource-response "index.html" {:root "public"}))

(defn async-handler
  ([request]
   {:status 200
    :headers {"Content-Type" "text/html"}
    :body "I came from an async handler."})
  ([request respond raise]
   (respond (async-handler request))))

(defn handler
  [request]
  {:status 200
   :headers {"Content-Type" "text/html"}
   :body "Hello, world!"})

;;;; Middleware

(defn wrap-content-type
  [handler content-type]
  (fn [request]
    (let [response (handler request)]
      (assoc-in response [:headers "Content-Type"] content-type))))


;;;; Main
(def PORT
  "Heroku requires that the server uses $PORT.
  See https://devcenter.heroku.com/articles/dynos#local-environment-variables"
  (try 
    (Integer. (System/getenv "PORT"))
    (catch Exception e
      (println (str "Caught exception: " (.getMessage e))))))

(defn -main
  [& args]
  (jetty/run-jetty (-> (reitit-ring/ring-handler 
                         router
                         (reitit-ring/routes 
                           (reitit-ring/create-resource-handler {:root "public"
                                                                 :path "/"})
                           (reitit-ring/create-file-handler     {:root "public"
                                                                 :path "/"})
                           (reitit-ring/create-default-handler)
                           ))
                       (wrap-reload))
                   {:port (or PORT 3000)
                    :join? false}))
