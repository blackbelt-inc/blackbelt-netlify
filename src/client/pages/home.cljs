(ns client.pages.home
  (:require [reagent.core :as reagent]))

(defn ^:export home-page
  []
  [:<>
   [:a {:class "hover:text-red-500"
        :href "/tasks/12398"} "task"]
   [:p {:class "text-xl"} "lolol"]
   [:h1 {:class "text-xs"} "home"]])
