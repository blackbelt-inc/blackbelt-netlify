(ns client.pages.task
  (:require [reagent.core :as reagent]
            [reagent.session :as session]))

(def state (reagent/atom {:x 0, :y 0, :state "default", :max-attempts 5, :count 0, :correct 0}))

(defn single-to-single
  []
  (swap! state assoc :x (rand-int 9))
  (swap! state assoc :y (- (+ (@state :x) (rand-int (- 9 (@state :x)))) (@state :x))))

(defn put-score! 
  [] 
  (session/put! :score-data {:score (@state :correct)}) 
  (swap! state assoc :state "end"))

(defn ^:export task-page
  []
  [:div {:class "relative"}
   [:h1 {:class "content-center"}
         (case (@state :state)
           "default"
           [:div "Press Start"]
           "start"
           [:div (@state :x) " + " (@state :y)]
           "check"
           [:div [:div (@state :x) " + " (@state :y)] " = " (+ (@state :x) (@state :y))]
           "end"
           [:div [:div [:div (@state :x) " + " (@state :y)] " = " (+ (@state :x) (@state :y))] "end of task"]
           "results" ""
           "")]
   (case (@state :state)
     "default"
     [:input {:type "button" :value "Start"
              :on-click #((single-to-single)
                          (swap! state assoc :state "start"))}]
     "start"
     [:input {:type "button" :value "show answer"
              :on-click #((if (= (@state :count) (@state :max-attempts)) 
                            (put-score!)
                            (swap! state assoc :count (+ 1 (@state :count))))
                          (swap! state assoc :state "check"))}]
     "check"
     [:div
      [:input {:type "button" :value "Incorrect"
               :on-click #((single-to-single)
                           (swap! state assoc :state "start"))}]
      [:input {:type "button" :value "Correct"
               :on-click #((swap! state assoc :correct (+ 1 (@state :correct)))
                           (single-to-single)
                           (swap! state assoc :state "start"))}]]
     "end"
     [:a {:href "/result"} "go to results"]
     [:div
      [:input {:type "button" :value "home"
               :on-click #(swap! state assoc :state "results")}]
      [:input {:type "button" :value "restart"
               :on-click #(swap! state assoc :state "results")}]])])
