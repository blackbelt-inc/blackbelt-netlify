(ns client.pages.result
  (:require [reagent.core :as reagent]
            [reagent.session :as session]))

(defn ^:export result-page
  []
  [:<>
   [:h1 {:class "text-xs"} "score " ((session/get :score-data) :score)]])
