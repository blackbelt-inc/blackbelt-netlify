(ns client.core
  (:require [reagent.core :as reagent]
            [reagent.session :as session]
            [accountant.core :as accountant]
            [reitit.frontend :as reitit]
            [client.routes :as routes]))

;;;; Helpers

(defn handle-navigation
  "What happens every time a user follows a link;
  in particular, deposits Reagent component corresponding to
  link URL into browser session"
  [path]
  (let [match        (reitit/match-by-path routes/router path)
        current-page (:name (:data match))
        route-params (:path-params match)]
    ;; Puts routing data into browser session
    (session/put! :route-data {:current-page (routes/page-for current-page)
                               :route-params route-params})))

(defn path-exists?
  "Checks whether route has corresponding path"
  [path]
  (boolean (reitit/match-by-path routes/router path)))

;;;; Main

;; TODO: add support for queries (i.e., "?foo=bar" in the URL)
(defn session-page
  "Get page from current browser session"
  []
  (let [route-data (session/get :route-data)
        page (:current-page route-data)
        params (:route-params route-data)]
    (if (seq params)
      [page params]
      [page])))

(defn ^:dev/after-load mount
  "Mounts the current session page to the app div"
  []
  (reagent/render-component [session-page]
                      (.getElementById js/document "client")))

(defn ^:export -main
  []

  ;; Set up navigation
  (accountant/configure-navigation!
    {:nav-handler handle-navigation
     :path-exists? path-exists?})

  ;; Update path using current URL
  (accountant/dispatch-current!) 

  ;; Render the corresponding Reagent component
  (mount))
