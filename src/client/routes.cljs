(ns client.routes
  (:require [reagent.core :as reagent]
            [reitit.frontend :as reitit]
            [client.pages.home :refer [home-page]]
            [client.pages.task :refer [task-page]]
            [client.pages.result :refer [result-page]]))

(def router
  (reitit/router
    [["/" ::home-page]
     ["/tasks"
      ["" ::tasks-page]
      ["/:task-id" ::task-page]]
     ["/about" ::about-page]
     ["/result" ::result-page]]))

(defn path-for
  "Given namespaced keyword for route, returns path
  EX: (path-for ::tasks-page) -> \"/tasks\""
  [route & [params]]
  (if params
    (:path (reitit/match-by-name router route params))
    (:path (reitit/match-by-name router route))))

(defn page-for
  "Given namespaced keyword for route, returns Reagent component"
  [route]
  (case route
    ::home-page #'home-page
    ::task-page #'task-page
    ::result-page #'result-page))
