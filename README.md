# Setup

Prerequisites: `npm install shadow-cljs reagent reagent-dom create-react-class` then `npm install`

To run the server: `clj -A:server`

To run the client: `npm run dev`

